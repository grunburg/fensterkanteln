<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="shortcut icon" type="image/png" href="assets/img/favicon.png"/>
		<title>Fensterkanteln - Kontakti</title>
	</head>
	<body>
		<div class="container">
			<div class="brand text-center">
				<object data="https://fensterkanteln.lv/assets/img/svg/fensterkan_logo.svg" type="image/svg+xml" width="28" height="30" class="mx-auto" alt="Fensterkanteln Logo"></object>
				<a class="navbar-brand text-center" href="#">Fensterkanteln</a>
			</div>
			<!-- Nav Fix -->
			<nav class="navbar navbar-expand-sm navbar-light">
				<button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse text-center" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item">
							<a class="nav-link active" href="contacts.php">Kontakti <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="about.php">Par</a>
						</li>
						<!--<li class="nav-item">
							<a class="nav-link" href="products.php" >Produkti</a>
						</li>-->
					</ul>
				</div>
			</nav>
			<!-- Nav Fix END -->
			<hr>
			<div class="content contacts">
				<div class="text-center"><h1 class="bg-yellow page-title">Kontakti</h1></div>
				<div class="row">
					<div class="col-md-6 text-light text-md-right">
						<h5>Kontaktpersona</h5>
						<p>Viesturs Garda<br><a class="text-light" href="tel:0037129255225">+371 29255225</a><br><a class="text-light" href="mailto:fensterkanteln.lv@gmail.com">fensterkanteln.lv@gmail.com</a></p>
					</div>
					<div class="col-md-6">
						<h5>Birojs</h5>
						<p>Rīga, Līves Iela 2a</p>
					</div>
					<div class="col-md-6 text-md-right">
						<h5>Ražotne</h5>
						<p>K/S "Gaiziņš"<br>Madonas novads, Bērzaunes pagasts</p>
					</div>
				</div>
			</div>
			<footer class="text-center">
				<div class="brand">
					<object data="https://fensterkanteln.lv/assets/img/svg/fensterkan_logo.svg" type="image/svg+xml" width="28" height="30" class="mx-auto" alt="Fensterkanteln Logo"></object>
				</div>
			</footer>
		</div>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
	</body>
</html>