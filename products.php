<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="assets/css/styles.css">
		<title>Fensterkanteln - Produkti</title>
	</head>
	<body>
		<div class="container">
			<div class="brand text-center">
				<object data="https://fensterkanteln.lv/assets/img/svg/fensterkan_logo.svg" type="image/svg+xml" width="28" height="30" class="mx-auto" alt="Fensterkanteln Logo"></object>
				<a class="navbar-brand text-center" href="#">Fensterkanteln</a>
			</div>
			<!-- Nav Fix -->
			<nav class="navbar navbar-expand-sm navbar-light">
				<button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse text-center" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item">
							<a class="nav-link" href="contacts">Kontakti</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="about">Par</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="products">Produkti <span class="sr-only">(current)</span></a>
						</li>
					</ul>
				</div>
			</nav>
			<!-- Nav Fix END -->
			<hr>
			<div class="content">
				<div class="text-center"><h1 class="bg-yellow">Produkti</h1></div>
				...
			</div>
			<footer class="text-center">
				<div class="brand">
					<img src="https://fensterkanteln.lv/assets/img/svg/fensterkan_logo.svg" width="28" height="30" class="align-self-start" alt="Fensterkanteln Logo">
				</div>
			</footer>
		</div>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
	</body>
</html>