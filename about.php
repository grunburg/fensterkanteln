<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="assets/css/styles.css">
		<title>Fensterkanteln - Par</title>
	</head>
	<body>
		<div class="container">
			<div class="brand text-center">
				<object data="https://fensterkanteln.lv/assets/img/svg/fensterkan_logo.svg" type="image/svg+xml" width="28" height="30" class="mx-auto" alt="Fensterkanteln Logo"></object>
				<a class="navbar-brand text-center" href="#">Fensterkanteln</a>
			</div>
			<!-- Nav Fix -->
			<nav class="navbar navbar-expand-sm navbar-light">
				<button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse text-center" id="navbarNav">
					<ul class="navbar-nav mx-auto">
						<li class="nav-item">
							<a class="nav-link" href="contacts">Kontakti</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="about">Par <span class="sr-only">(current)</span></a>
						</li>
						<!--<li class="nav-item">
							<a class="nav-link" href="products" >Produkti</a>
						</li>-->
					</ul>
				</div>
			</nav>
			<!-- Nav Fix END -->
			<hr>
			<div class="content">
				<div class="text-center"><h1 class="bg-yellow page-title">Par</h1></div>
				<div class="col-md-8 mx-auto">
					<div class="card text-center">
						<div class="card-body">
							<h1 class="card-title">Profesionāļi Profesionāļiem</h1>
							<p class="card-text">Fensterkanteln piedāvā iegādāties pašu ražotas priedes koka brusas logu un durvju ražošanai.</p>
							<a class="btn btn-outline-dark disabled" href="products.php" role="button">Produkti</a>
						</div>
					</div>
					<hr>
				</div>
				<div class="col-md-8 text-center mx-auto mt-5">
					<p>Mēs esam nozares profesionāļi, kas augsti specializējušies garumā audzētu priedes koka līmētu logu brusu ražošanā.</p>
				</div>
				<div class="col-md-8 text-center mx-auto mt-5">
					<p>Mēs zinām, ka arī Jūs esat savas nozares profesionāļi un tāpēc novērtēsiet mūsu vairāk kā 20 gadu ilgo pieredzi šī produkta ražošanā un tirdzniecībā. Sadarbojoties ar mums Jūs iegūsiet uzticamu partneri, kas precīzi piegādās Jums nepieciešamo produkciju. Mūsu ražošanas jaudas sniegs Jums garantiju pasūtījumu savlaicīgā izpildē, kā arī pielietojamā tehnoloģija garantēs produkcijas kvalitāti.</p>
				</div>
				<div class="col-md-8 text-center mx-auto mt-5">
					<p>Fensterkanteln ražotās logu brusas ir atzinīgi novērtējuši logu un durvju ražotāji daudzās Eiropas valstīs.</p>
				</div>
			</div>
			<footer class="text-center">
				<div class="brand">
					<img src="https://fensterkanteln.lv/assets/img/svg/fensterkan_logo.svg" width="28" height="30" class="align-self-start" alt="Fensterkanteln Logo">
				</div>
			</footer>
		</div>
		<script type="text/javascript" src="lib/node_modules/jquery/dist/jquery.min.js"></script>
		<script type="text/javascript" src="lib/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	</body>
</html>